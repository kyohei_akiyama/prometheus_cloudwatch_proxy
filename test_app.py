# -*- coding: utf-8 -*-

import os
import re

from nose.tools import ok_
from nose.plugins.skip import SkipTest
from webtest import TestApp
from time import time
from app import wsgi_app

BILLING_METRICS = [
    'aws_billing_EstimatedCharges'
]
RDS_METRICS = [
    'aws_rds_CPUUtilization',
    'aws_rds_FreeableMemory',
    'aws_rds_FreeStorageSpace',
    'aws_rds_SwapUsage',
    'aws_rds_ReadIOPS',
    'aws_rds_WriteIOPS',
    'aws_rds_ReadLatency',
    'aws_rds_WriteLatency',
]
AURORA_CLUSTER_METRICS = [
    'aws_rds_VolumeBytesUsed',
    'aws_rds_VolumeWriteIOPs',
    'aws_rds_VolumeReadIOPs',
]
AURORA_INSTANCE_METRICS = [
    'aws_rds_CPUUtilization',
    'aws_rds_FreeableMemory',
    'aws_rds_EngineUptime',
    'aws_rds_FreeLocalStorage',
]
ElastiCache_METRICS = [
    'aws_elasticache_CPUUtilization',
    'aws_elasticache_FreeableMemory',
    'aws_elasticache_NetworkBytesIn',
    'aws_elasticache_NetworkBytesOut',
    'aws_elasticache_GetTypeCmds',
    'aws_elasticache_SetTypeCmds',
]
DynamoDB_METRICS = [
    'aws_dynamodb_ProvisionedReadCapacityUnits',
    'aws_dynamodb_ProvisionedWriteCapacityUnits',
    r'aws_dynamodb_ProvisionedReadCapacityUnits\{.*index=.*\}',
    r'aws_dynamodb_ProvisionedWriteCapacityUnits\{.*index=.*\}',
]
SES_METRICS = [
    r'aws_ses_quota\{region=".*", name="Max24HourSend"\}',
    r'aws_ses_quota\{region=".*", name="MaxSendRate"\}',
    r'aws_ses_quota\{region=".*", name="SentLast24Hours"\}',
]


def get_old_start_time(sec_before=600):
    now = int(time())
    return now - sec_before


def test_get_billing_metrics():
    if os.environ['BILLING_ACCOUNT_ID'] == '':
        raise SkipTest

    url = '/metrics?profile=%s&namespace=AWS/Billing&account_id=%s' % (
        os.environ['PROFILE_NAME'], os.environ['BILLING_ACCOUNT_ID'])

    app = TestApp(wsgi_app)
    res = app.get(url)

    ok_(res.status_int == 200)
    stats = res.body.decode('utf-8').split('\n')
    for m in BILLING_METRICS:
        rule = re.compile(m)
        ok_(len([s for s in stats if rule.match(s)]) >= 1)


def test_get_rds_metrics():
    if os.environ['RDS_INSTANCE_ID'] == '':
        raise SkipTest

    is_aurora = os.environ['IS_AURORA'] != ''
    url = ('/metrics?profile=%s&namespace=AWS/RDS'
           '&dimensions=Name=DBInstanceIdentifier,Value=%s' % (
               os.environ['PROFILE_NAME'], os.environ['RDS_INSTANCE_ID']))
    url += '&start_time=%s' % get_old_start_time()
    if is_aurora:
        url += '&is_aurora=1'

    app = TestApp(wsgi_app)
    res = app.get(url)

    ok_(res.status_int == 200)
    stats = res.body.decode('utf-8').split('\n')
    for m in (RDS_METRICS if not is_aurora else AURORA_INSTANCE_METRICS):
        rule = re.compile(m)
        ok_(len([s for s in stats if rule.match(s)]) >= 1)


def test_get_aurora_cluster_metrics():
    if os.environ['AURORA_CLUSTER_ID'] == '':
        raise SkipTest

    url = ('/metrics?profile=%s&namespace=AWS/RDS'
           '&dimensions=Name=DbClusterIdentifier,Value=%s'
           '%%20Name=EngineName,Value=aurora&period=300' % (
               os.environ['PROFILE_NAME'], os.environ['AURORA_CLUSTER_ID']))
    url += '&start_time=%s' % get_old_start_time()

    app = TestApp(wsgi_app)
    res = app.get(url)

    ok_(res.status_int == 200)
    stats = res.body.decode('utf-8').split('\n')
    for m in AURORA_CLUSTER_METRICS:
        rule = re.compile(m)
        ok_(len([s for s in stats if rule.match(s)]) >= 1)


def test_get_elasticache_metrics():
    if os.environ['CACHE_CLUSTER_ID'] == '':
        raise SkipTest

    url = ('/metrics?profile=%s&namespace=AWS/ElastiCache'
           '&dimensions=Name=CacheClusterId,Value=%s' % (
               os.environ['PROFILE_NAME'], os.environ['CACHE_CLUSTER_ID']))
    url += '&start_time=%s' % get_old_start_time()

    app = TestApp(wsgi_app)
    res = app.get(url)

    ok_(res.status_int == 200)
    stats = res.body.decode('utf-8').split('\n')
    for m in ElastiCache_METRICS:
        rule = re.compile(m)
        ok_(len([s for s in stats if rule.match(s)]) >= 1)


def test_get_dynamodb_metrics():
    if os.environ['DYNAMODB_PREFIX'] == '':
        raise SkipTest

    url = '/metrics?profile=%s&namespace=AWS/DynamoDB&prefix=%s' % (
        os.environ['PROFILE_NAME'], os.environ['DYNAMODB_PREFIX'])
    url += '&start_time=%s' % get_old_start_time()

    app = TestApp(wsgi_app)
    res = app.get(url)

    ok_(res.status_int == 200)
    stats = res.body.decode('utf-8').split('\n')
    for m in DynamoDB_METRICS:
        rule = re.compile(m)
        ok_(len([s for s in stats if rule.match(s)]) >= 1)


def test_get_ses_metrics():
    if os.environ['SES_REGION'] == '':
        raise SkipTest

    url = '/metrics?profile=%s&namespace=AWS/SES&region=%s' % (
        os.environ['PROFILE_NAME'], os.environ['SES_REGION'])

    app = TestApp(wsgi_app)
    res = app.get(url)

    ok_(res.status_int == 200)
    stats = res.body.decode('utf-8').split('\n')
    for m in SES_METRICS:
        rule = re.compile(m)
        ok_(len([s for s in stats if rule.match(s)]) >= 1)
