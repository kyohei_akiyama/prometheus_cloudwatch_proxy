# -*- coding: utf-8 -*-

import os
import sys
import traceback
from time import time, mktime
from copy import deepcopy
from boto3 import Session
from bottle import Bottle, run, request, response

NAME_PREFIX = os.getenv('NAME_PREFIX', default='aws')
RECORDING_INTERVAL = {
    'AWS/Billing': 300,
    'AWS/RDS': 60,
    'AWS/ElastiCache': 60,
    'AWS/DynamoDB': 60,
    'AWS/SES': 0
}
METRICS = {
    'AWS/Billing': [
        {'name': 'EstimatedCharges', 'statistics': 'Maximum'}
    ],
    'AWS/RDS': [
        {'name': 'CPUUtilization', 'statistics': 'Average'},
        {'name': 'FreeableMemory', 'statistics': 'Average'},
        {'name': 'FreeStorageSpace', 'statistics': 'Average'},
        {'name': 'SwapUsage', 'statistics': 'Average'},
        {'name': 'ReadIOPS', 'statistics': 'Average'},
        {'name': 'WriteIOPS', 'statistics': 'Average'},
        {'name': 'ReadLatency', 'statistics': 'Average'},
        {'name': 'WriteLatency', 'statistics': 'Average'},
    ],
    'AWS/ElastiCache': [
        {'name': 'CPUUtilization', 'statistics': 'Average'},
        {'name': 'FreeableMemory', 'statistics': 'Average'},
        {'name': 'NetworkBytesIn', 'statistics': 'Average'},
        {'name': 'NetworkBytesOut', 'statistics': 'Average'},
        {'name': 'GetTypeCmds', 'statistics': 'Average'},
        {'name': 'SetTypeCmds', 'statistics': 'Average'},
    ],
    'AWS/DynamoDB': [
        {'name': 'ConsumedReadCapacityUnits', 'statistics': 'Sum'},
        {'name': 'ConsumedWriteCapacityUnits', 'statistics': 'Sum'},
        {'name': 'ReadThrottleEvents', 'statistics': 'Sum'},
        {'name': 'WriteThrottleEvents', 'statistics': 'Sum'},
        {'name': 'UserErrors', 'statistics': 'Sum'},
        {'name': 'SystemErrors', 'statistics': 'Sum'},
        {'name': 'ProvisionedReadCapacityUnits', 'statistics': 'Average'},
        {'name': 'ProvisionedWriteCapacityUnits', 'statistics': 'Average'}
    ]
}
AURORA_CLUSTER_METRICS = [
    {'name': 'VolumeBytesUsed', 'statistics': 'Average'},
    {'name': 'VolumeWriteIOPs', 'statistics': 'Average'},
    {'name': 'VolumeReadIOPs', 'statistics': 'Average'},
]
AURORA_INSTANCE_METRICS = [
    {'name': 'AuroraBinlogReplicaLag', 'statistics': 'Average'},
    {'name': 'AuroraReplicaLag', 'statistics': 'Average'},
    {'name': 'AuroraReplicaLagMaximum', 'statistics': 'Average'},
    {'name': 'AuroraReplicaLagMinimum', 'statistics': 'Average'},
    {'name': 'BlockedTransactions', 'statistics': 'Average'},
    {'name': 'BufferCacheHitRatio', 'statistics': 'Average'},
    {'name': 'CPUCreditBalance', 'statistics': 'Average'},
    {'name': 'CPUCreditUsage', 'statistics': 'Average'},
    {'name': 'CommitLatency', 'statistics': 'Average'},
    {'name': 'CommitThroughput', 'statistics': 'Average'},
    {'name': 'DDLLatency', 'statistics': 'Average'},
    {'name': 'DDLThroughput', 'statistics': 'Average'},
    {'name': 'DMLLatency', 'statistics': 'Average'},
    {'name': 'DMLThroughput', 'statistics': 'Average'},
    {'name': 'Deadlocks', 'statistics': 'Average'},
    {'name': 'DeleteLatency', 'statistics': 'Average'},
    {'name': 'DeleteThroughput', 'statistics': 'Average'},
    {'name': 'EngineUptime', 'statistics': 'Average'},
    {'name': 'FreeLocalStorage', 'statistics': 'Average'},
    {'name': 'InsertLatency', 'statistics': 'Average'},
    {'name': 'InsertThroughput', 'statistics': 'Average'},
    {'name': 'LoginFailures', 'statistics': 'Average'},
    {'name': 'NetworkThroughput', 'statistics': 'Average'},
    {'name': 'Queries', 'statistics': 'Average'},
    {'name': 'ResultSetCacheHitRatio', 'statistics': 'Average'},
    {'name': 'SelectLatency', 'statistics': 'Average'},
    {'name': 'SelectThroughput', 'statistics': 'Average'},
    {'name': 'UpdateLatency', 'statistics': 'Average'},
    {'name': 'UpdateThroughput', 'statistics': 'Average'},
]
BILLING_SERVICES = [
    'AmazonEC2',
    'AmazonRDS',
    'AmazonElastiCache',
    'AmazonS3',
    'AmazonCloudFront',
    'AmazonDynamoDB',
    'AWSDataTransfer',
    'AmazonRoute53',
    'AmazonSNS',
    'AmazonCloudWatch',
    'Total'
]
SES_QUOTAS = [
    'Max24HourSend',
    'MaxSendRate',
    'SentLast24Hours'
]


def parse_dimensions(dimensions):
    ret = []
    for dimension in dimensions.split(' '):
        ret.append({d.split('=')[0]: d.split('=')[1]
                    for d in dimension.split(',')})
    return ret


def get_conn(profile, service='cloudwatch', region=None):
    if region:
        sess = Session(profile_name=profile, region_name=region)
    else:
        sess = Session(profile_name=profile)
    return sess.client(service)


def get_dynamodb_gsis(conn, table):
    api_res = conn.describe_table(TableName=table)
    gsis = [g['IndexName'] for g in
            api_res['Table'].get('GlobalSecondaryIndexes', [])]
    return gsis


def get_dynamodb_tables_by_prefix(conn, prefix, last_table=None):
    ret = []
    api_res = conn.list_tables()

    for table in [t for t in api_res['TableNames'] if t.startswith(prefix)]:
        gsis = get_dynamodb_gsis(conn, table)
        ret.append({'name': table, 'gsis': gsis})

    if 'LastEvaluatedTableName' in api_res:
        ret.extend(conn, prefix, last_table=api_res['LastEvaluatedTableName'])

    return ret


def dp_to_prometheus_metric(datapoints, namespace,
                            metric_name, statistics,
                            add_labels=None, add_timestamp=True):
    label = 'statistics="%s"' % statistics.lower()
    for l, v in (add_labels.items() if add_labels else {}.items()):
        label += ', %s="%s"' % (l, v)

    ret = []
    for dp in sorted(datapoints, key=lambda v: v['Timestamp']):
        line = '%s_%s_%s{%s} %s' % (
            NAME_PREFIX, namespace.split('/')[1].lower(), metric_name,
            label, dp[statistics])
        if add_timestamp:
            line += ' %s' % (int(mktime(dp['Timestamp'].timetuple())) * 1000)
        ret.append(line)
    return ret


def get_cloudwatch_metrics(conn, namespace,
                           dimensions, metric_name, statistics,
                           start_time, end_time, period):
    api_res = conn.get_metric_statistics(
        Namespace=namespace, MetricName=metric_name,
        Dimensions=dimensions, Statistics=[statistics],
        StartTime=start_time, EndTime=end_time, Period=period)
    return api_res.get('Datapoints', [])


def get_basic_service_metrics(conn, namespace, dimensions, metrics,
                              start_time, end_time, period):
    ret = []
    for metric_name, statistics in [
            (m['name'], m['statistics'],) for m in metrics]:
        datapoints = get_cloudwatch_metrics(conn, namespace, dimensions,
                                            metric_name, statistics,
                                            start_time, end_time, period)
        ret.extend(dp_to_prometheus_metric(
            datapoints, namespace, metric_name, statistics))
    return ret


def get_billing_metrics(conn, namespace, account_id,
                        start_time, end_time, period):
    ret = []
    for metric_name, statistics in [
            (m['name'], m['statistics'],) for m in METRICS[namespace]]:
        for service in BILLING_SERVICES:
            dimensions = [
                {'Name': 'Currency', 'Value': 'USD'},
                {'Name': 'LinkedAccount', 'Value': account_id}
            ]
            if service != 'Total':
                dimensions.append({'Name': 'ServiceName', 'Value': service})

            datapoints = get_cloudwatch_metrics(conn, namespace, dimensions,
                                                metric_name, statistics,
                                                start_time, end_time, period)
            ret.extend(dp_to_prometheus_metric(
                datapoints, namespace, metric_name, statistics,
                add_labels={'service': service}, add_timestamp=False))
    return ret


def get_dynamodb_metrics(conn, namespace, tables,
                         start_time, end_time, period):
    ret = []
    for table, gsis in [(t['name'], t['gsis'],) for t in tables]:
        dimensions = [{'Name': 'TableName', 'Value': table}]

        for metric_name, statistics in [
                (m['name'], m['statistics'],) for m in METRICS[namespace]]:
            # Get table metrics.
            datapoints = get_cloudwatch_metrics(conn, namespace, dimensions,
                                                metric_name, statistics,
                                                start_time, end_time, period)
            ret.extend(dp_to_prometheus_metric(
                datapoints, namespace, metric_name, statistics,
                add_labels={'table': table}))

            # Get global secondary index metrics.
            for gsi in gsis:
                gsi_dimensions = deepcopy(dimensions)
                gsi_dimensions.append(
                    {'Name': 'GlobalSecondaryIndexName', 'Value': gsi})

                datapoints = get_cloudwatch_metrics(
                    conn, namespace, gsi_dimensions,
                    metric_name, statistics,
                    start_time, end_time, period)
                ret.extend(dp_to_prometheus_metric(
                    datapoints, namespace, metric_name, statistics,
                    add_labels={'table': table, 'index': gsi}))
    return ret


def get_ses_metrics(conn, region):
    ret = []
    api_res = conn.get_send_quota()
    for quota in SES_QUOTAS:
        m = '%s_ses_quota{region="%s", name="%s"} %s' % (
            NAME_PREFIX, region, quota, api_res[quota])
        ret.append(m)
    return ret


wsgi_app = Bottle()


@wsgi_app.get('/metrics')
def metrics_handler():
    try:
        query = request.query

        profile = query['profile']
        namespace = query['namespace']

        period = int(query.get('period') or RECORDING_INTERVAL[namespace])
        now = int(time())
        end_time = int(query.get('end_time') or (now - period))
        start_time = int(query.get('start_time') or (end_time - period))

        conn = get_conn(profile)

        if namespace == 'AWS/Billing':
            start_time = end_time - 14400
            account_id = query['account_id']
            metrics = get_billing_metrics(conn, namespace, account_id,
                                          start_time, end_time, period)
        elif namespace == 'AWS/DynamoDB':
            # Because it tends to be missing, we also get past data.
            start_time = end_time - 300
            dyconn = get_conn(profile, service='dynamodb')
            tables = get_dynamodb_tables_by_prefix(dyconn, query['prefix'])
            metrics = get_dynamodb_metrics(conn, namespace, tables,
                                           start_time, end_time, period)
        elif namespace == 'AWS/SES':
            sesconn = get_conn(profile, service='ses', region=query['region'])
            metrics = get_ses_metrics(sesconn, query['region'])
        else:
            dimensions = query['dimensions']
            if 'DbClusterIdentifier' in dimensions:
                # Because it tends to be missing, we also get past data.
                start_time -= 3600
                metric_names = list(AURORA_CLUSTER_METRICS)
            else:
                metric_names = list(METRICS[namespace])
                if namespace == 'AWS/RDS' and 'is_aurora' in query.keys():
                    metric_names.extend(list(AURORA_INSTANCE_METRICS))

            metrics = get_basic_service_metrics(
                conn, namespace, parse_dimensions(dimensions), metric_names,
                start_time, end_time, period)

        response.content_type = 'text/plain'
        return '\n'.join(metrics) + '\n'
    except:
        response.status = 500
        st = traceback.format_exc()
        sys.stderr.write(st)
        return st


if __name__ == '__main__':
    run(wsgi_app, host='localhost', port=8080)
