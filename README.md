## Installing dependent packages
```shell
pip install bottle boto3
```

or

```shell
pip install -r requirements.txt
```

## Setup AWS credentials
```shell
$ cat > ~/.aws/credentials <<EOF
[app1]
region = ap-northeast-1
aws_access_key_id = your_api_key
aws_secret_access_key = your_api_secret

[app2]
region = ap-northeast-1
aws_access_key_id = your_api_key
aws_secret_access_key = your_api_secret
EOF
```

## Running
```shell
python app.py
```

## Running with Gunicorn
```shell
gunicorn -b 0.0.0.0:8000 app:wsgi_app
```

## Usage
### Get Billing metrics
```shell
$ curl 'http://127.0.0.1:8000/metrics?profile=billing&namespace=AWS/Billing&account_id=0123456789'
aws_billing_EstimatedCharges{statistics="maximum", service="AmazonEC2"} 302.22
aws_billing_EstimatedCharges{statistics="maximum", service="AmazonRDS"} 5.12
aws_billing_EstimatedCharges{statistics="maximum", service="AmazonElastiCache"} 3.54
aws_billing_EstimatedCharges{statistics="maximum", service="AmazonS3"} 0.18
aws_billing_EstimatedCharges{statistics="maximum", service="AmazonDynamoDB"} 0.2
aws_billing_EstimatedCharges{statistics="maximum", service="AWSDataTransfer"} 3.76
aws_billing_EstimatedCharges{statistics="maximum", service="AmazonRoute53"} 5.28
aws_billing_EstimatedCharges{statistics="maximum", service="Total"} 320.29
```

### Get RDS(engine=MySQL) metrics
```shell
$ curl 'http://127.0.0.1:8000/metrics?profile=app1&namespace=AWS/RDS&dimensions=Name=DBInstanceIdentifier,Value=instance_id'
aws_rds_FreeableMemory{statistics="average"} 13370658816.0 1489366080
aws_rds_FreeStorageSpace{statistics="average"} 910381846528.0 1489366080
aws_rds_SwapUsage{statistics="average"} 0.0 1489366080
aws_rds_ReadIOPS{statistics="average"} 0.13333333333333333 1489366080
aws_rds_WriteIOPS{statistics="average"} 247.2 1489366080
aws_rds_ReadLatency{statistics="average"} 0.001 1489366080
aws_rds_WriteLatency{statistics="average"} 0.0009358524116470327 1489366080
```

### Get RDS(engine=Aurora) metrics
```shell
$ curl 'http://127.0.0.1:8000/metrics?profile=app1&namespace=AWS/RDS&dimensions=Name=DBInstanceIdentifier,Value=instance_id&is_aurora=1'
aws_rds_CPUUtilization{statistics="average"} 5.08 1491096540000
aws_rds_FreeableMemory{statistics="average"} 5196234752.0 1491096540000
aws_rds_AuroraBinlogReplicaLag{statistics="average"} 0.0 1491096540000
aws_rds_AuroraReplicaLagMaximum{statistics="average"} 19.43600082397461 1491096540000
aws_rds_AuroraReplicaLagMinimum{statistics="average"} 19.43600082397461 1491096540000
aws_rds_BlockedTransactions{statistics="average"} 0.0 1491096540000
aws_rds_BufferCacheHitRatio{statistics="average"} 100.0 1491096540000
aws_rds_CommitLatency{statistics="average"} 6.1577 1491096540000
aws_rds_CommitThroughput{statistics="average"} 0.5003502451716202 1491096540000
aws_rds_DDLLatency{statistics="average"} 0.0 1491096540000
aws_rds_DDLThroughput{statistics="average"} 0.0 1491096540000
aws_rds_DMLLatency{statistics="average"} 0.22196666666666667 1491096540000
aws_rds_DMLThroughput{statistics="average"} 0.5003502451716202 1491096540000
aws_rds_Deadlocks{statistics="average"} 0.0 1491096540000
aws_rds_DeleteLatency{statistics="average"} 0.0 1491096540000
aws_rds_DeleteThroughput{statistics="average"} 0.0 1491096540000
aws_rds_EngineUptime{statistics="average"} 436620.0 1491096540000
aws_rds_FreeLocalStorage{statistics="average"} 30903205888.0 1491096540000
aws_rds_InsertLatency{statistics="average"} 0.22196666666666667 1491096540000
aws_rds_InsertThroughput{statistics="average"} 0.5003502451716202 1491096540000
aws_rds_LoginFailures{statistics="average"} 0.0 1491096540000
aws_rds_NetworkThroughput{statistics="average"} 3010.7507167144477 1491096540000
aws_rds_Queries{statistics="average"} 6.988691330019682 1491096540000
aws_rds_ResultSetCacheHitRatio{statistics="average"} 0.0 1491096540000
aws_rds_SelectLatency{statistics="average"} 0.30609774436090226 1491096540000
aws_rds_SelectThroughput{statistics="average"} 2.218219420260849 1491096540000
aws_rds_UpdateLatency{statistics="average"} 0.0 1491096540000
aws_rds_UpdateThroughput{statistics="average"} 0.0 1491096540000
```

### Get Aurora cluster metrics
```shell
http://127.0.0.1:8000/metrics?profile=app1&namespace=AWS/RDS&dimensions=Name=DbClusterIdentifier,Value=cluster_id%20Name=EngineName,Value=aurora&period=300
aws_rds_VolumeBytesUsed{statistics="average"} 79250915328.0 1491096120000
aws_rds_VolumeWriteIOPs{statistics="average"} 1998.0 1491096120000
aws_rds_VolumeReadIOPs{statistics="average"} 0.0 1491096120000
```

### Get ElastiCache metrics
```shell
$ curl 'http://127.0.0.1:8000/metrics?profile=app1&namespace=AWS/ElastiCache&dimensions=Name=CacheClusterId,Value=cluster_id'
aws_elasticache_CPUUtilization{statistics="average"} 7.25 1489366140
aws_elasticache_FreeableMemory{statistics="average"} 29589176320.0 1489366140
aws_elasticache_NetworkBytesIn{statistics="average"} 54521899.0 1489366140
aws_elasticache_NetworkBytesOut{statistics="average"} 74427090.0 1489366140
aws_elasticache_GetTypeCmds{statistics="average"} 125783.0 1489366140
aws_elasticache_SetTypeCmds{statistics="average"} 21546.0 148936614
```

### Get DynamoDB metrics
```shell
$ curl -s 'http://127.0.0.1:8000/metrics?profile=cmn&namespace=AWS/DynamoDB&prefix=app1-prod' | head
aws_dynamodb_ProvisionedReadCapacityUnits{statistics="average", table="app1-prod-auth-csession"} 10.0 1491491700000
aws_dynamodb_ProvisionedWriteCapacityUnits{statistics="average", table="app1-prod-auth-csession"} 10.0 1491491700000
aws_dynamodb_ProvisionedReadCapacityUnits{statistics="average", table="app1-prod-auth-cuser"} 10.0 1491491700000
aws_dynamodb_ProvisionedWriteCapacityUnits{statistics="average", table="app1-prod-auth-cuser"} 10.0 1491491700000
aws_dynamodb_ProvisionedReadCapacityUnits{statistics="average", table="app1-prod-auth-cuser-appuser"} 10.0 1491491700000
aws_dynamodb_ProvisionedReadCapacityUnits{statistics="average", table="app1-prod-auth-cuser-appuser", index="appid-created-time-index"} 10.0 1491491700000
aws_dynamodb_ProvisionedReadCapacityUnits{statistics="average", table="app1-prod-auth-cuser-appuser", index="appid-appuid-index"} 10.0 1491491700000
aws_dynamodb_ProvisionedWriteCapacityUnits{statistics="average", table="app1-prod-auth-cuser-appuser"} 10.0 1491491700000
aws_dynamodb_ProvisionedWriteCapacityUnits{statistics="average", table="app1-prod-auth-cuser-appuser", index="appid-created-time-index"} 10.0 1491491700000
aws_dynamodb_ProvisionedWriteCapacityUnits{statistics="average", table="app1-prod-auth-cuser-appuser", index="appid-appuid-index"} 10.0 1491491700000
```

### Get SES Metrics
```shell
$ curl -s 'http://127.0.0.1::8000/metrics?profile=app1&namespace=AWS/SES&region=us-west-2'
aws_ses_quota{region="us-west-2", name="Max24HourSend"} 50000.0
aws_ses_quota{region="us-west-2", name="MaxSendRate"} 14.0
aws_ses_quota{region="us-west-2", name="SentLast24Hours"} 4.0
```

## Test
```shell
# Testing RDS instance.
PROFILE_NAME=app1 RDS_INSTANCE_ID=hoge tox

# Testing Aurora instance.
PROFILE_NAME=app1 RDS_INSTANCE_ID=hoge IS_AURORA=yes tox

# Testing Aurora cluster.
PROFILE_NAME=app1 AURORA_CLUSTER_ID=hoge tox

# Testing ElastiCache instance.
PROFILE_NAME=app1 CACHE_CLUSTER_ID=hoge tox
```
